package controllers;

import models.*;
import play.mvc.*;
import play.data.Form;
import java.util.*;
import views.html.*;
import views.html.students.*;
import com.avaje.ebean.Ebean;
import play.mvc.Security;
@Security.Authenticated(Secured.class)
public class Students extends Controller
{
    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list() {
        List<Student> students = Student.findAll();
        return ok(list.render(students));
    }

    public static Result newStudent() {
        return ok(details.render(studentForm));
    }

    public static Result details(Student student) {
        if (student == null) {
            return notFound(String.format("Student %s does not exist.", student.mssv));
        }	
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));
    }
    public static Result delete(String mssv) {
        final Student student = Student.findByMSSV(mssv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.",mssv));
        }
        student.delete();
        return redirect(routes.Students.list());
    }
    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Student student = boundForm.get();
        Ebean.save(student);
        student.save();
        flash("success", String.format("Successfully added student %s", student));
        return redirect(routes.Students.list());
    }
}
