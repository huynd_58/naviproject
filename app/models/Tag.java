package models;

import java.util.List;

public class Tag {
    public Long id;
    public String name;
    public List<Student> students;
}