package models;
import models.Student;
public class StockItem {
    public Warehouse warehouse;           // trường quan hệ nối với Warehouse
    public Student student;               // trường quan hệ nối với Student
    public Long quantity;

    public String toString() {
        return String.format("$d %s", quantity, student);
    }
}