package models;
import play.data.validation.Constraints;
import java.util.*;
import play.mvc.*;
import play.data.validation.Constraints;
import play.mvc.PathBindable;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.mvc.Security;

@Entity
public class Student extends Model implements PathBindable<Student>
{

    @Id
    public Long id;
    @Constraints.Required
    public String stt;
    @Constraints.Required
    public String ten;
    @Constraints.Required
    public String ngaysinh;
    @Constraints.Required
    public String mssv;
    @Constraints.Required
    public String diachi;
    @Constraints.Required
    public String email;
    public String pass; // Chua su dung

    public byte[] picture;
    public List<Tag> tags = new LinkedList<Tag>();
    //public List<Tag> tags;

    public Student() {}
    public Student(String stt, String ten, String ngaysinh, String mssv, String diachi, String email)
    {
        this.stt = stt;
        this.ten = ten;
        this.ngaysinh = ngaysinh;
        this.mssv = mssv;
        this.diachi = diachi;
        this.email = email;
    }

    private static List<Student> students;

    public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);
    public static List<Student> findAll() {
        return find.all();
    }

    public static Student findByEmail(String email) {
        for (Student candicate : students) {
            if (candicate.email.equals(email)) {
                return candicate;
            }
        }
        return null;
    }

    public static Student findByMSSV(String mssv) {
        return find.where().eq("mssv", mssv).findUnique();
    }
    public static List<Student> findByName(String term) {
        final List<Student> results = new ArrayList<Student>();
        for (Student candidate : students) {
            if (candidate.ten.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return null;
    }

    public static boolean remove(Student student) {
        return students.remove(student);
    }

    public void save() {
        students.remove(findByEmail(this.email));
        students.add(this);
    }
	
	@Override
   public Student bind(String key, String value) {
	   return findByMSSV(value);
   }
   	@Override
   public String unbind(String key) {
       return mssv;
   }
	 
	@Override
   public String javascriptUnbind() {
       return mssv;
   }
	
    public String toString() {
        return String.format("  %s  -  MSSV: %s", ten, mssv);
    }
}
