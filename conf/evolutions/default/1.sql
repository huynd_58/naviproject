# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  id                        bigint not null,
  stt                       varchar(255),
  ten                       varchar(255),
  ngaysinh                  varchar(255),
  mssv                      varchar(255),
  diachi                    varchar(255),
  email                     varchar(255),
  pass                      varchar(255),
  picture                   varbinary(255),
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence student_seq;

create sequence user_account_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists student;

drop table if exists user_account;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

